<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Biodata</title>
</head>
<body>
    <h1>View Dari Controller</h1>
    <h4>Parsing data from Controller to view</h4>
    <h1> Nama : {{$nama}}</h1>

    <ul>
        @foreach ($kerja as $item)
            <li>{{$item}}</li>
        @endforeach
    </ul>

</body>
</html>