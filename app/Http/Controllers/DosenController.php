<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DosenController extends Controller
{
    //
    public function index()
    {
        $nama       = "syaiful";
        $pekerjaan   = ["Manager Astra International", "Bank Negara Indonesia", "Bank Central Asia (BCA)"];

        return view('biodata', ['nama' => $nama, 'kerja' => $pekerjaan]);
    }
}
